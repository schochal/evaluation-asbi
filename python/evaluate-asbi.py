import numpy as np
import pandas as pd
import os
import sys


DATA="/home/alexander_schoch/Documents/Asbi/evaluation-asbi/data/0506"
OUTPUT="/home/alexander_schoch/Documents/Asbi/evaluation-asbi/output/0506-1.csv"

compounds = ["NH3", "O2", "N2", "N2O", "NO", "Ar"]

if(len(sys.argv) > 1 and sys.argv[1] == "--gui"):
    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk


    class GUI(Gtk.Window):
        def __init__(self):
            Gtk.Window.__init__(self, title="Evaluation of Asbis Data")
            self.set_border_width(50)
            #self.set_default_size(1000, 800)

            mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.add(mainbox)

            self.files(mainbox)
            self.INPUT = "None"
            self.OUTPUT = "None"

            button_run = Gtk.Button(label="Run!")
            button_run.connect("clicked", self.run)
            mainbox.pack_start(button_run, True, True, 0)

        def files(self, mainbox):
            # INPUT FILE
            lab_input = Gtk.Label(label="Choose a folder to evaluate")
            inputfolder_dialog = Gtk.FileChooserDialog(title="Input Folder", action=Gtk.FileChooserAction.SELECT_FOLDER, parent=self)
            inputfolder_dialog.add_buttons(
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK,
            )
            inputfolder_dialog.set_current_folder("~")
            inputfolder_button = Gtk.FileChooserButton(title="Input Folder", dialog=inputfolder_dialog)
            inputfolder_button.connect("file-set", self.on_input_file_set)
            mainbox.pack_start(lab_input, True, True, 0)
            mainbox.pack_start(inputfolder_button, True, True, 0)

            # OUTPUT FILE
            lab_output = Gtk.Label(label="Choose the folder in which to save the output")
        
            self.outputfolder_button = Gtk.Button(label="Output Folder")
            self.outputfolder_button.connect("clicked", self.on_output_file_set)
            mainbox.pack_start(lab_output, True, True, 0)
            mainbox.pack_start(self.outputfolder_button, True, True, 0)

        def on_input_file_set(self, widget):
            self.INPUT = widget.get_filename()

        def on_output_file_set(self, widget):
            outputfolder_dialog = Gtk.FileChooserDialog(title="Output Folder", action=Gtk.FileChooserAction.SAVE, parent=self)
            outputfolder_dialog.add_buttons(
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK,
            )

            response = outputfolder_dialog.run()
            if response == Gtk.ResponseType.OK:
                self.OUTPUT = outputfolder_dialog.get_filename()
            else:
                self.notify_user("Error", "Something has gone wrong")

            outputfolder_dialog.destroy()
            self.outputfolder_button.set_label(self.OUTPUT.rsplit('/', 1)[1])


        def notify_user(self, message, secondary="None"):
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.INFO,
                buttons=Gtk.ButtonsType.OK,
                text=message
            )
            if(secondary!="None"):
                dialog.secondary_text = secondary
            dialog.run()

            dialog.destroy()
        

        def run(self, widget):
            if(self.INPUT != "None" and self.OUTPUT != "None"):
                evaluate(self.INPUT, self.OUTPUT)
                self.notify_user("Written output to " + self.OUTPUT)
            else:
                self.notify_user("Please set input and output")
             

def evaluate(INPUT, OUTPUT):
    # GET ALL THE SUBDIRECTORIES
    directories = os.listdir(DATA)
    identifiers = sorted([s.rsplit("-", 1)[1].rsplit(".", 1)[0] for s in directories])
    root = directories[0].rsplit("-", 1)[0]

    # GENERATE AN EMPTY DATA FRAME FOR ALL THE DATA
    empty_data = np.zeros(len(identifiers) * len(compounds))
    output_data = pd.DataFrame(empty_data.reshape(-1, 6))
    output_data.columns = compounds
    output_data.index = identifiers

    # LOOP THROUGH ALL SUBDIRECTORIES
    for i in identifiers:
        path = DATA + "/" + root + "-" + i + ".D" + "/RESULTS.CSV"
        print("  reading " + root + "-" + i + ".D")

        file = open(path)
        content = file.readlines()
        critical_line = content[12]

        values = np.zeros(6)
        line_nr = 12
        for k in range(len(values)):
            line = content[line_nr]
            # HOW THIS WORKS: IF THE VALUE IS FOUND, WRITE IT TO VALUES
            #   IF NOT, WRITE A 0, SKIP THE LINE AND PROCEED BY 3
            try:
                values[k] = line.split(',')[8]
                line_nr += 4
            except Exception:
                print("  --> A Value of 0 for " + compounds[k] + " was found")
                values[k] = 0
                line_nr += 3
        for j in range(len(compounds)):
            output_data[compounds[j]][i] = values[j]

    # SAVE TO OUTPUT FILE
    output_data.transpose().to_csv(OUTPUT)

if(len(sys.argv) > 1 and sys.argv[1] == "--gui"):
    print("Starting in GUI mode")
    win = GUI()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
else:
    print("Using CLI")
    if(len(sys.argv) == 3):
        DATA=sys.argv[1]
        OUTPUT=sys.argv[2]
    else:
        print("Please enter the data folder:")
        DATA=input("> ")
        print()
        print("Please enter the output file name:")
        OUTPUT=input("> ")
    evaluate(DATA, OUTPUT)
    print("Data written to", OUTPUT)
