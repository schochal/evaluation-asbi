import numpy as np
import pandas as pd
import os
import sys

from PyQt5.QtWidgets import (QMainWindow, QAction, QFileDialog, QApplication, QLabel)
from PyQt5.QtGui import QIcon

DATA="/home/alexander_schoch/Documents/Asbi/evaluation-asbi/data/0506"
OUTPUT="/home/alexander_schoch/Documents/Asbi/evaluation-asbi/output/0506-1.csv"

compounds = ["NH3", "O2", "N2", "N2O", "NO", "Ar"]

class GUI(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        lab = QLabel("Select a folder to evaluate", self)
        self.setGeometry(300, 300, 450, 350)
        self.setWindowTitle('Input dialog')
        lab.show()


def evaluate(INPUT, OUTPUT):
    # GET ALL THE SUBDIRECTORIES
    directories = os.listdir(DATA)
    identifiers = sorted([s.rsplit("-", 1)[1].rsplit(".", 1)[0] for s in directories])
    root = directories[0].rsplit("-", 1)[0]

    # GENERATE AN EMPTY DATA FRAME FOR ALL THE DATA
    empty_data = np.zeros(len(identifiers) * len(compounds))
    output_data = pd.DataFrame(empty_data.reshape(-1, 6))
    output_data.columns = compounds
    output_data.index = identifiers

    # LOOP THROUGH ALL SUBDIRECTORIES
    for i in identifiers:
        path = DATA + "/" + root + "-" + i + ".D" + "/RESULTS.CSV"

        file = open(path)
        content = file.readlines()
        critical_line = content[12]

        # CHECK IF THERE IS AMMONIA AT THE END OF REACTOR
        if(critical_line.split(",")[0] == "1="):
            # THERE IS AMMONIA AT THE END OF REACTOR
            lines_of_interest = [13, 17, 21, 25, 29, 33]
            values = [content[l-1].split(',')[8] for l in lines_of_interest]
            for j in range(len(compounds)):
                output_data[compounds[j]][i] = values[j]
        else:
            # NO AMMONIA AT THE END OF REACTOR --> NH3 = 0
            lines_of_interest = [16, 20, 24, 28, 32]
            values = [content[l-1].split(',')[8] for l in lines_of_interest]
            output_data['NH3'][i] = 0
            for j in range(1, len(compounds)):
                output_data[compounds[j]][i] = values[j-1]

    # SAVE TO OUTPUT FILE
    output_data.transpose().to_csv(OUTPUT)

if(len(sys.argv) > 1 and sys.argv[1] == "--gui"):
    print("Starting in GUI mode")
    app = QApplication(sys.argv)
    gui = GUI()
    sys.exit(app.exec_())
else:
    print("Using CLI")
    if(len(sys.argv) == 3):
        DATA=sys.argv[1]
        OUTPUT=sys.argv[2]
    else:
        print("Please enter the data folder:")
        DATA=input("> ")
        print()
        print("Please enter the output file name:")
        OUTPUT=input("> ")
    print("Data written to", OUTPUT)
    evaluate(DATA, OUTPUT)
