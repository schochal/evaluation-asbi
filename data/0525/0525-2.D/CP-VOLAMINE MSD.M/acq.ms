[Signature]
ID=00000000000000000000000000000000

[Version]
ID=G1701CA Version: C.00.00

[General]
Type=3
Inlet=0
Start Mode=0
Run Duration=39000.00
Solvent Delay=0.00
Tune File=atune.u
Group Ions=1
Acq Mode=0
Gauges=2
Fast Scan=0

[Zone 1]
ID=1
On=1
Soft Max=200
Set Point=150
Type=0

[Zone 2]
ID=0
On=1
Soft Max=250
Set Point=230
Type=0

[Zone 3]
ID=-1
On=0
Soft Max=0
Set Point=0
Type=0

[Ions 1]
Group Name=1
Start Time=0.00
Resolution=1
Plot Index 1=0
Plot Index 2=0
Number of Ions=1
Ion 1=74.10,100

[Scan 1]
Start Time=0.00
Low Mass=10.00
High Mass=50.00
Threshold=150
Sampling=4
Low Plot 1=50.00
High Plot 1=550.00
Low Plot 2=10.00
High Plot 2=60.00

[Scan 2]
Start Time=-1.00
Low Mass=50.00
High Mass=550.00
Threshold=150
Sampling=4
Low Plot 1=50.00
High Plot 1=550.00
Low Plot 2=50.00
High Plot 2=550.00

[Scan 3]
Start Time=-1.00
Low Mass=50.00
High Mass=550.00
Threshold=150
Sampling=4
Low Plot 1=50.00
High Plot 1=550.00
Low Plot 2=50.00
High Plot 2=550.00

[Filters]
Mass Type=0
Mass Param=0
Mass Sigma=0.00
Time Type=0
Time Param=0
Time Sigma=0.00

[Plot]
Plot Type 1=0
Time Plot 1=3
Min Plot 1=0
Max Plot 1=1500000
Left Plot 1=0.274219
Top Plot 1=0.365934
Width Plot 1=0.724219
Height Plot 1=0.594505
Mode Plot 1=1
Plot Type 2=2
Time Plot 2=3
Min Plot 2=0
Max Plot 2=100000
Left Plot 2=0.004687
Top Plot 2=0.357143
Width Plot 2=0.274219
Height Plot 2=0.605495
Mode Plot 2=1
X Gadget Pos 1=683
Y Gadget Pos 1=163
X Gadget Pos 2=686
Y Gadget Pos 2=231

[Gauge1]
Type=2
ID=1
Display Type=0
Left Plot=545
Top Plot=229
Alarm Error=100.00
Alarm Warning=100.00
Alarm Minimum=0.00
Alarm Set=0

[Gauge2]
Type=2
ID=0
Display Type=0
Left Plot=546
Top Plot=165
Alarm Error=100.00
Alarm Warning=100.00
Alarm Minimum=0.00
Alarm Set=0


[Parameters]
ID 1=7
Mode 1=2
Value 1=1.000000
ID 2=-1
Mode 2=0
Value 2=0.000000
[Time Events]
Number Events=0
