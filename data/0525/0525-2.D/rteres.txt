                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0525\
  Data File : 0525-2.D                                            
  Acq On    : 25 May 2021  13:18
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.986  2307 2347 2451   M2  223229   3307179 100.00% 100.000%
 
 
                        Sum of corrected areas:     3307179
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.893  2195 2233 2326   M2  664202   8291147 100.00% 100.000%
 
 
                        Sum of corrected areas:     8291147
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.887  2200 2224 2252   M2    6399     72078 100.00% 100.000%
 
 
                        Sum of corrected areas:       72078
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.967  2293 2324 2355   M2    8363     93849 100.00% 100.000%
 
 
                        Sum of corrected areas:       93849
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.897  2222 2237 2254   M2     155        75 100.00% 100.000%
 
 
                        Sum of corrected areas:          75
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0525-2.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.897  2197 2237 2343   M2  520025   6572028 100.00% 100.000%
 
 
                        Sum of corrected areas:     6572028

CP-VOLAMINE MSD.M Tue May 25 15:54:38 2021   
