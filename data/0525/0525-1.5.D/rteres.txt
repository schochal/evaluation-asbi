                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0525\
  Data File : 0525-1.5.D                                          
  Acq On    : 25 May 2021  13:13
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.986  2308 2347 2447   M2  231727   3365970 100.00% 100.000%
 
 
                        Sum of corrected areas:     3365970
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.893  2196 2232 2321   M2  688040   8607274 100.00% 100.000%
 
 
                        Sum of corrected areas:     8607274
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.887  2201 2225 2256   M2    6731     77470 100.00% 100.000%
 
 
                        Sum of corrected areas:       77470
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.968  2293 2325 2353   M2    7951     93193 100.00% 100.000%
 
 
                        Sum of corrected areas:       93193
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.895  2222 2235 2258   M2     171        83 100.00% 100.000%
 
 
                        Sum of corrected areas:          83
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0525-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2196 2236 2343   M2  540954   6791534 100.00% 100.000%
 
 
                        Sum of corrected areas:     6791534

CP-VOLAMINE MSD.M Tue May 25 15:52:33 2021   
