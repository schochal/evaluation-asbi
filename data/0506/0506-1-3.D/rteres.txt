                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0506\
  Data File : 0506-1-3.D                                          
  Acq On    :  6 May 2021  17:32
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.987  2330 2349 2373   M5    3092     37756 100.00% 100.000%
 
 
                        Sum of corrected areas:       37756
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.895  2202 2234 2271   M2   68034    845852 100.00% 100.000%
 
 
                        Sum of corrected areas:      845852
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.885  2193 2222 2263   M2   89591   1060483 100.00% 100.000%
 
 
                        Sum of corrected areas:     1060483
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.968  2281 2325 2412   M2  401659   4905751 100.00% 100.000%
 
 
                        Sum of corrected areas:     4905751
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.899  2205 2240 2272   M2     984     11211 100.00% 100.000%
 
 
                        Sum of corrected areas:       11211
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0506-1-3.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2196 2236 2347   M3  557158   6932621 100.00% 100.000%
 
 
                        Sum of corrected areas:     6932621

CP-VOLAMINE MSD.M Thu May 06 18:00:49 2021   
