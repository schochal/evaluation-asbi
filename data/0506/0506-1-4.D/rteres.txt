                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0506\
  Data File : 0506-1-4.D                                          
  Acq On    :  6 May 2021  18:06
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
 
     No peaks were detected using the method integration parameters!
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.893  2203 2232 2273   M2   42287    528246 100.00% 100.000%
 
 
                        Sum of corrected areas:      528246
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.886  2194 2224 2264   M2   72694    855195 100.00% 100.000%
 
 
                        Sum of corrected areas:      855195
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.967  2277 2323 2435   M2  430227   5278014 100.00% 100.000%
 
 
                        Sum of corrected areas:     5278014
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.898  2194 2238 2281   M2    3936     45758 100.00% 100.000%
 
 
                        Sum of corrected areas:       45758
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0506-1-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2199 2236 2345   M4  547815   6867277 100.00% 100.000%
 
 
                        Sum of corrected areas:     6867277

CP-VOLAMINE MSD.M Fri May 07 10:55:14 2021   
