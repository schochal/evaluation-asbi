                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0518\
  Data File : 0518-1-1.5.D                                        
  Acq On    : 18 May 2021  12:45
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.987  2310 2348 2423   M2  119630   1663313 100.00% 100.000%
 
 
                        Sum of corrected areas:     1663313
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.894  2196 2233 2313   M2  383545   4774970 100.00% 100.000%
 
 
                        Sum of corrected areas:     4774970
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.887  2201 2225 2254   M2    6951     82948 100.00% 100.000%
 
 
                        Sum of corrected areas:       82948
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.966  2291 2323 2359   M3   13062    149717 100.00% 100.000%
 
 
                        Sum of corrected areas:      149717
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.891  2217 2230 2247   M2     154        75 100.00% 100.000%
 
 
                        Sum of corrected areas:          75
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0518-1-1.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.898  2195 2238 2341   M2  317435   3935915 100.00% 100.000%
 
 
                        Sum of corrected areas:     3935915

CP-VOLAMINE MSD.M Tue May 25 15:38:26 2021   
