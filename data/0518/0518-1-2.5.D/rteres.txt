                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0518\
  Data File : 0518-1-2.5.D                                        
  Acq On    : 18 May 2021  13:41
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0503\0503-1.5.D\CP-VOLAMINE MSD.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.987  2314 2348 2397   M3   75309   1083722 100.00% 100.000%
 
 
                        Sum of corrected areas:     1083722
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.895  2195 2234 2309   M2  272774   3411867 100.00% 100.000%
 
 
                        Sum of corrected areas:     3411867
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.886  2194 2223 2275   M2   13258    168746 100.00% 100.000%
 
 
                        Sum of corrected areas:      168746
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.967  2290 2324 2386   M3   83937    996070 100.00% 100.000%
 
 
                        Sum of corrected areas:      996070
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.898  2195 2238 2280   M2    1607     16999 100.00% 100.000%
 
 
                        Sum of corrected areas:       16999
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0518-1-2.5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.897  2197 2237 2339   M3  312900   3896150 100.00% 100.000%
 
 
                        Sum of corrected areas:     3896150

CP-VOLAMINE MSD.M Tue May 25 15:39:39 2021   
