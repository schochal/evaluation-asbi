                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0413\
  Data File : 0413-7.D                                            
  Acq On    : 13 Apr 2021  19:35
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0318\0318-h01.D\CP-VOLAMINE MSD---60MIN.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.985  2304 2346 2422   M2  224577   3142650 100.00% 100.000%
 
 
                        Sum of corrected areas:     3142650
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.893  2186 2232 2336   M2  638275   8009094 100.00% 100.000%
 
 
                        Sum of corrected areas:     8009094
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.884  2185 2221 2269   M2   10995    120850 100.00% 100.000%
 
 
                        Sum of corrected areas:      120850
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.965  2284 2321 2377   M2   16960    194180 100.00% 100.000%
 
 
                        Sum of corrected areas:      194180
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2218 2235 2252   M2     486      3271 100.00% 100.000%
 
 
                        Sum of corrected areas:        3271
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0413-7.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2192 2236 2341   M2  553337   6946220 100.00% 100.000%
 
 
                        Sum of corrected areas:     6946220

CP-VOLAMINE MSD---60MIN.M Tue Apr 13 19:50:38 2021   
