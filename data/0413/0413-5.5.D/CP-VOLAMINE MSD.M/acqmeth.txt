                                        INSTRUMENT CONTROL PARAMETERS:    OXYHALOGENATION
                                        -------------------------------------------------

   C:\MSDCHEM\1\METHODS\CP-Volamine MSD.M
      Fri Apr 09 17:59:57 2021

Control Information
------- -----------

Sample Inlet      :  GC
Injection Source  :  Valve/Immediate Start
Mass Spectrometer :  Enabled


 No Sample Prep method has been assigned to this method.

=============================================================================
                                6890 GC METHOD
=============================================================================


OVEN
   Initial temp:  120 'C (On)              Maximum temp:  265 'C
   Initial time:  3.00 min                 Equilibration time:  0.50 min
   Ramps:
      #  Rate  Final temp  Final time
      1   0.0(Off)
   Post temp:  0 'C
   Post time:  0.00 min
   Run time:  3.00 min


FRONT INLET (SPLIT/SPLITLESS)           BACK INLET (UNKNOWN)
   Mode:  Split
   Initial temp:  220 'C (On)
   Pressure:  0.197 bar (On)
   Split ratio:  50:1
   Split flow:  59.9 mL/min
   Total flow:  63.9 mL/min
   Gas saver:  Off
   Gas type:  Helium


COLUMN 1                                COLUMN 2
   Capillary Column                        Capillary Column
   Model Number:  J&W 123-1031             Model Number:  J&W 113-4362
   DB-1                                    GS-GasPro
   Max temperature:  325 'C                Max temperature:  260 'C
   Nominal length:  30.0 m                 Nominal length:  60.0 m
   Nominal diameter:  320.00 um            Nominal diameter:  320.00 um
   Nominal film thickness:  0.10 um        Nominal film thickness:  0.00 um
   Mode:  constant flow                    Inlet:  (unspecified)
   Initial flow:  1.2 mL/min               Outlet:  Front Detector
   Nominal init pressure:  0.197 bar
   Average velocity:  41 cm/sec
   Inlet:  Front Inlet
   Outlet:  MSD
   Outlet pressure:  vacuum


FRONT DETECTOR (TCD)                    BACK DETECTOR (NO DET)
   Temperature:  200 'C (Off)
   Reference flow:  20.0 mL/min (Off)
   Mode:  Constant makeup flow
   Makeup flow:  7.0 mL/min (Off)
   Makeup Gas Type: Helium
   Filament:  Off
   Negative polarity:  Off


SIGNAL 1                                SIGNAL 2
   Data rate:  5 Hz                        Data rate:  10 Hz
   Type:  front detector                   Type:  test plot
   Save Data:  Off                         Save Data:  Off
   Zero:  0.0 (Off)                        Zero:  0.0 (Off)
   Range:  0                               Range:  0
   Fast Peaks:  Off                        Fast Peaks:  Off
   Attenuation:  0                         Attenuation:  0


COLUMN COMP 1                           COLUMN COMP 2
   (No Detectors Installed)                (No Detectors Installed)


THERMAL AUX 1                           THERMAL AUX 2
   Use:  Valve Box Heater                  Use:  MSD Transfer Line Heater
   Description:  Valve Box                 Description:  MSD Transferline
   Initial temp:  200 'C (On)              Initial temp:  250 'C (On)
   Initial time:  0.00 min                 Initial time:  0.00 min
      #  Rate  Final temp  Final time         #  Rate  Final temp  Final time
      1   0.0(Off)                            1   0.0(Off)


VALVES                                  POST RUN
   Valve 1  Gas Sampling                   Post Time: 0.00 min
      Description:  Gassampling
      Loop Volume: 0.025 mL
      Load Time: 0.50 min
      Inject Time: 0.50 min
      Inlet:  Front Inlet


TIME TABLE
   Time       Specifier                     Parameter & Setpoint




                               GC Injector


     Front Injector:
No parameters specified

     Back Injector:
No parameters specified

 Column 1 Inventory Number : CP-Volamine
 Column 2 Inventory Number : GasPro 6

                                MS ACQUISITION PARAMETERS


General Information
------- -----------

Tune File                : atune.u
Acquistion Mode          : Scan


MS Information
-- -----------

Solvent Delay            : 0.00 min

EMV Mode                 : Gain Factor
Gain Factor              : 1.00
Resulting EM Voltage     : 1506

[Scan Parameters]

Low Mass                 : 10.0
High Mass                : 50.0
Threshold                : 150
Sample #                 : 2       A/D Samples    4
Plot 2 low mass          : 10.0
Plot 2 high mass         : 60.0

[MSZones]

MS Source                : 230 C   maximum 250 C
MS Quad                  : 150 C   maximum 200 C


                             END OF MS ACQUISITION PARAMETERS


                              TUNE PARAMETERS for SN: US00000001
                        -----------------------------

 Trace Ion Detection is OFF.

 EMISSION    :      34.610
 ENERGY      :      69.922
 REPELLER    :      18.731
 IONFOCUS    :      90.157
 ENTRANCE_LE :      19.000
 EMVOLTS     :    1482.353
                               Actual EMV  :    1505.88
                               GAIN FACTOR :       0.97
 AMUGAIN     :    2726.000
 AMUOFFSET   :     129.000
 FILAMENT    :       2.000
 DCPOLARITY  :       0.000
 ENTLENSOFFS :      13.804
 MASSGAIN    :     595.000   
 MASSOFFSET  :     -11.000   

                           END OF TUNE PARAMETERS
                      ------------------------------------



                                               END OF INSTRUMENT CONTROL PARAMETERS
                                               ------------------------------------
