                                                          Area Percent Report

  Data Path : C:\Zhenchen\Ammox test 2021\0413\
  Data File : 0413-4.D                                            
  Acq On    : 13 Apr 2021  16:35
  Operator  : manager
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: events.e
  Integrator: ChemStation

  Method    : C:\Zhenchen\Ammox test 2021\0318\0318-h01.D\CP-VOLAMINE MSD---60MIN.M
  Title     :  

  Signal     : EIC Ion  17.00 (16.70 to 17.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.986  2311 2347 2403   M3   33543    477922 100.00% 100.000%
 
 
                        Sum of corrected areas:      477922
  Signal     : EIC Ion  32.00 (31.70 to 32.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.894  2193 2233 2315   M2  471363   5877054 100.00% 100.000%
 
 
                        Sum of corrected areas:     5877054
  Signal     : EIC Ion  28.00 (27.70 to 28.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.884  2185 2221 2270   M2   21157    238192 100.00% 100.000%
 
 
                        Sum of corrected areas:      238192
  Signal     : EIC Ion  44.00 (43.70 to 44.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.966  2285 2323 2387   M3  148374   1767059 100.00% 100.000%
 
 
                        Sum of corrected areas:     1767059
  Signal     : EIC Ion  30.00 (29.70 to 30.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.894  2207 2233 2268   M2    4063     44837 100.00% 100.000%
 
 
                        Sum of corrected areas:       44837
  Signal     : EIC Ion  40.00 (39.70 to 40.70): 0413-4.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1   1.896  2194 2236 2330   M3  560947   6970516 100.00% 100.000%
 
 
                        Sum of corrected areas:     6970516

CP-VOLAMINE MSD---60MIN.M Tue Apr 13 19:29:00 2021   
